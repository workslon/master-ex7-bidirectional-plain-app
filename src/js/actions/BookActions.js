  var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var validator = require('validator');
var XHR = require('xhr-promise');
var xhr = new XHR();

xhr.defaults = {
  url: 'https://public-library-api2.herokuapp.com/api/classes/Book/',
  headers: {
    'X-Parse-Application-Id': 'MyH7zW1'
  }
};

xhr.defaults.publisherUrl = xhr.defaults.url.replace('Book', 'Publisher');

module.exports = {
  getBooks: function () {
    var promise = xhr.GET({
      url: xhr.defaults.url + '?include=publisher',
    });

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_BOOKS,
      success: AppConstants.REQUEST_BOOKS_SUCCESS,
      failure: AppConstants.REQUEST_BOOKS_ERROR
    });

    return promise;
  },

  createBook: function (data) {
    AppDispatcher.dispatch({
      type: AppConstants.REQUEST_BOOK_SAVE
    });

    validator.validate(data);

    if (validator.isValid()) {
      xhr.GET({
        url: xhr.defaults.url + '?where={"isbn":"' + data.isbn + '"}'
      })
      .then(function (result) {
        try {
          if (JSON.parse(result).results.length) {
            AppDispatcher.dispatch({
              type: AppConstants.NON_UNIQUE_ISBN
            });
          } else {
            // publisher
            var operation;

            if (data.publisher && data.publisher.objectId) {
              data.publisher.__type = 'Pointer';
              data.publisher.className = 'Publisher';
              operation = 'AddRelation';
            } else {
              data.publisher = undefined;
            }

            xhr
              .POST({data: data})
              .then(function (result) {
                if (operation) {
                  xhr.PUT({
                    url: xhr.defaults.publisherUrl + data.publisher.objectId,
                    data: {
                      publishedBooks: {
                        __op: 'AddRelation',
                        objects: [{
                          __type: 'Pointer',
                          className: 'Book',
                          objectId: JSON.parse(result).objectId
                        }]
                      }
                    }
                  }).then(function (success) {
                      AppDispatcher.dispatch({
                        type: AppConstants.BOOK_SAVE_SUCCESS,
                        result: result,
                        data: data
                      })
                    }, function (error) {
                      AppDispatcher.dispatch({
                        type: AppConstants.BOOK_SAVE_SUCCESS,
                        result: result,
                        data: data
                      })
                    });
                } else {
                  AppDispatcher.dispatch({
                    type: AppConstants.BOOK_SAVE_SUCCESS,
                    result: result,
                    data: data
                  })
                }
              }, function (error) {
                throw new Error(error);
              });
          }
        } catch(e) {}
      },

      function (error) {
        alert('Server error!');
      });
    } else {
      AppDispatcher.dispatch({
        type: AppConstants.BOOK_VALIDATION_ERROR,
        errors: validator.errors
      });
    }
  },

  updateBook: function (book, newData) {
    var bookPromise;
    var publisherPromise;
    var operation;

    validator.validate(newData);

    if (validator.isValid()) {
      if (newData.publisher && newData.publisher.objectId) {
        newData.publisher.__type = 'Pointer';
        newData.publisher.className = 'Publisher';
        operation = 'AddRelation';
      } else {
        newData.publisher = {
          __op: 'Delete'
        }

        book.publisher && (operation = 'RemoveRelation');
      }

      // update book
      bookPromise = xhr.PUT({
        url: xhr.defaults.url + book.objectId,
        data: newData
      });

      newData.objectId = book.objectId;

      AppDispatcher.dispatchAsync(bookPromise, {
        request: AppConstants.REQUEST_BOOK_UPDATE,
        success: AppConstants.BOOK_UPDATE_SUCCESS,
        failure: AppConstants.BOOK_UPDATE_ERROR
      }, newData);

      // update publisher
      if (operation) {
        var publisherId = (book.publisher || newData.publisher).objectId;
        publisherPromise = xhr.PUT({
          url: xhr.defaults.publisherUrl + publisherId,
          data: {
            publishedBooks: {
              __op: operation,
              objects: [{
                __type: 'Pointer',
                className: 'Book',
                objectId: book.objectId
              }]
            }
          }
        });

        AppDispatcher.dispatchAsync(publisherPromise, {
          request: AppConstants.REQUEST_PUBLISHER_UPDATE,
          success: AppConstants.PUBLISHER_UPDATE_SUCCESS,
          failure: AppConstants.PUBLISHER_UPDATE_ERROR
        }, newData);
      }
    } else {
      AppDispatcher.dispatch({
        type: AppConstants.BOOK_VALIDATION_ERROR,
        errors: validator.errors
      });
    }
  },

  deleteBook: function (book) {
    AppDispatcher.dispatch({
      type: AppConstants.REQUEST_BOOK_DESTROY
    });

    if (book.publisher) {
      var promise = xhr.DELETE({
        url: xhr.defaults.url + book.objectId
      });

      AppDispatcher.dispatchAsync(promise, {
        request: AppConstants.REQUEST_BOOK_DESTROY,
        success: AppConstants.BOOK_DESTROY_SUCCESS,
        failure: AppConstants.BOOK_DESTROY_ERROR
      }, book);

      // delete inverse reference
      var publisherPromise = xhr.PUT({
        url: xhr.defaults.publisherUrl + book.publisher.objectId,
        data: {
          publishedBooks: {
            __op: 'RemoveRelation',
            objects: [{
              __type: 'Pointer',
              className: 'Book',
              objectId: book.objectId
            }]
          }
        }
      });

      AppDispatcher.dispatchAsync(publisherPromise, {
        request: AppConstants.REQUEST_PUBLISHER_DESTROY,
        success: AppConstants.PUBLISHER_DESTROY_SUCCESS,
        failure: AppConstants.PUBLISHER_DESTROY_ERROR
      });
    } else {
      var promise = xhr.DELETE({
        url: xhr.defaults.url + book.objectId
      });

      AppDispatcher.dispatchAsync(promise, {
        request: AppConstants.REQUEST_BOOK_DESTROY,
        success: AppConstants.BOOK_DESTROY_SUCCESS,
        failure: AppConstants.BOOK_DESTROY_ERROR
      }, book);
    }
  }
};